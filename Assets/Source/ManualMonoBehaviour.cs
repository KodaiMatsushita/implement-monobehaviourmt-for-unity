﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ManualMonoBehaviour : MonoBehaviour,IManualMonoBehaviour
{
    public abstract void Initialize(); //Awakeの代わり
    public abstract void FirstRun(); //Startの代わり
    public abstract void Run(); //Updateの代わり
}
