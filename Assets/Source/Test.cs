﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Start()とUpdate()使いたくない
public class Test : ManualMonoBehaviour
{
    private float _time = 0;

    [SerializeField]
    private float _speed = 1.0f;

    public override void Initialize()
    {

    }

    public override void FirstRun()
    {

    }

    public override void Run()
    {
        var pos = this.transform.position;
        pos.x = Mathf.Sin(_speed * _time);
        transform.position = pos;
        _time += Time.deltaTime;
    }
}
