﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour {

    [SerializeField]
    ManualMonoBehaviour[] _gameObjects = null;

	// 各オブジェクトのスタートを呼ぶ順番を決める
	void Start ()
    {
        if (_gameObjects.Length == 0)
        {
            this.enabled = false;
            this.gameObject.SetActive(false);
            return;
        }
        foreach(var obj in _gameObjects)
        {
            obj.FirstRun();
        }
	}
	
	// 各オブジェクトの更新を呼ぶ順番を決める
	void Update ()
    {
        foreach(var obj in _gameObjects)
        {
            obj.Run();
        }
	}
}
