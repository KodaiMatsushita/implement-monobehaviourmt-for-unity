﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IManualMonoBehaviour
{
    void Initialize(); //Awakeの代わり
    void FirstRun(); //Startの代わり
    void Run(); //Updateの代わり
}
